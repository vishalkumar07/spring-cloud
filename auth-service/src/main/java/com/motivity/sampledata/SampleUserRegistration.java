package com.motivity.sampledata;

import com.motivity.model.User;
import com.motivity.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SampleUserRegistration {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleUserRegistration.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostConstruct
    private void postConstruct() {
        User user = new User();
        user.setName("test");
        user.setUserName("test");
        user.setEmail("test@test");
        user.setPassword(bCryptPasswordEncoder.encode("test"));
        userRepository.save(user);
        LOGGER.info("Inserted sample data for the user: " + "[ UserName: test, Password: test ]");
    }
}
