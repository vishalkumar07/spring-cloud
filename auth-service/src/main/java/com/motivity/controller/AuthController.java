package com.motivity.controller;

import com.motivity.model.AuthenticationRequest;
import com.motivity.model.AuthenticationResponse;
import com.motivity.model.User;
import com.motivity.service.AuthService;
import com.motivity.service.MyUserDetailsService;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @PostMapping("/basic")
    public ResponseEntity generateTokenForBasicAuth(@RequestBody AuthenticationRequest authenticationRequest) {
        try {
            return ResponseEntity.ok(authService.generateTokenForBasicAuth(authenticationRequest));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/validate")
    public ResponseEntity validateToken(@RequestBody AuthenticationResponse authenticationResponse) {
        try {
            return ResponseEntity.ok(authService.validateJwtToken(authenticationResponse.getJwt()));
        } catch (Exception e) {
            return new ResponseEntity<>(false +"\n"+ e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/register")
    public ResponseEntity registration(@RequestBody User user) {
        try {
            myUserDetailsService.createUser(user);
        } catch (Exception e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                return new ResponseEntity("Registration Failed, User already exists", HttpStatus.CONFLICT);
            } else if(e.getCause() instanceof PropertyValueException) {
                return new ResponseEntity("Invalid Json", HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity("Registration Success", HttpStatus.OK);
    }
}
