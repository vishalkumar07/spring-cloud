package com.motivity.service;

import com.motivity.model.AuthenticationRequest;
import com.motivity.model.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    public AuthenticationResponse generateTokenForBasicAuth(AuthenticationRequest authenticationRequest) throws Exception {
        /* authenticating user if authentication fails we will throw an exception */
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException badCredentialsException) {
            throw new Exception("Incorrect Password", badCredentialsException);
        } catch (InternalAuthenticationServiceException internalAuthenticationServiceException) {
            throw new Exception("User doesn't exist", internalAuthenticationServiceException);
        }
        /* if authentication is success we are generating jwt token with UserName and returning */
        return new AuthenticationResponse(
                jwtUtil.generateToken(myUserDetailsService.loadUserByUsername(authenticationRequest.getUserName()))
        );
    }

    public Boolean validateJwtToken(String jwtToken) throws Exception {
        return jwtUtil.validateToken(jwtToken);
    }
}
