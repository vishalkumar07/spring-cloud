package com.motivity.service;

import com.motivity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.motivity.model.User user = userRepository.findByUserName(username);
        return new User(user.getUserName(), user.getPassword(), new ArrayList<>());
    }

    public com.motivity.model.User createUser(com.motivity.model.User user) throws Exception{
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }
}
