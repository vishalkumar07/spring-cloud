package com.motivity.service;

import io.jsonwebtoken.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Service
public class JwtUtil {

    private static final String SECRET_KEY = "ApacheKaraf";

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder()
                .addClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public boolean validateToken(String authToken) throws Exception {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException signatureException) {
            throw new Exception("Invalid JWT signature.", signatureException);
        } catch (MalformedJwtException malformedJwtException) {
            throw new Exception("Invalid JWT token.", malformedJwtException);
        } catch (ExpiredJwtException expiredJwtException) {
            throw new Exception("Expired JWT token.", expiredJwtException);
        } catch (UnsupportedJwtException unsupportedJwtException) {
            throw new Exception("Unsupported JWT token.", unsupportedJwtException);
        } catch (IllegalArgumentException illegalArgumentException) {
            throw new Exception("JWT token compact of handler are invalid.", illegalArgumentException);
        }
    }
}
