APIs:
------------------------------------------------------------------------------------------------------------------------
user registration API:

Method: POST
URL: http://localhost:9100/auth/register

headers:
Content-Type: application/json

body:
{
  "name":"",
  "userName":"",
  "email":"",
  "password":""
}

------------------------------------------------------------------------------------------------------------------------
generate jwt token API:

Method: POST
URL: http://localhost:9100/auth/basic

headers:
Content-Type: application/json

body:
{
  "userName":"",
  "password":""
}

------------------------------------------------------------------------------------------------------------------------
validate jwt token API:

Method: POST
URL: http://localhost:9100/auth/validate

headers:
Content-Type: application/json

body:
{
"jwt": ""
}

------------------------------------------------------------------------------------------------------------------------