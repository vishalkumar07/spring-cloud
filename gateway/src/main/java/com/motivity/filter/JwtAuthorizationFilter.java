package com.motivity.filter;

import com.motivity.model.JwtValidationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class JwtAuthorizationFilter extends AbstractGatewayFilterFactory<JwtAuthorizationFilter.Config> {

    @Autowired
    private WebClient.Builder webClientBuilder;

    public JwtAuthorizationFilter() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(JwtAuthorizationFilter.Config config) {
        return (exchange, chain) -> {
            if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
                return onError(exchange, "Missing Authorization Header", HttpStatus.FORBIDDEN);
            }
            String authorizationHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);

            try {
                if (!isAuthorizationValid(authorizationHeader)) {
                    return onError(exchange, "Invalid Authorization Header", HttpStatus.FORBIDDEN);
                }
            } catch (ExecutionException | InterruptedException e) {
                return onError(exchange, e.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
            }
            return chain.filter(exchange);
        };
    }

    private Boolean isAuthorizationValid(String authorizationHeader) throws ExecutionException, InterruptedException {
        String jwt = null;
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
        }
        CompletableFuture<Boolean> future = webClientBuilder.build()
                .post()
                .uri("http://auth-service/auth/validate")
                .body(Mono.just(new JwtValidationRequest(jwt)), JwtValidationRequest.class)
                .retrieve()
                .bodyToMono(Boolean.class)
                .subscribeOn(Schedulers.boundedElastic())
                .timeout(Duration.ofSeconds(10)) // for now changing it to 10 seconds to avoid failures
                .onErrorResume(throwable -> Mono.empty())
                .toFuture();

        boolean result = future.get();
        return result;
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        System.out.println("error: " +err);
        return response.setComplete();
    }

    public static class Config {
        // this class is to put the configuration properties
    }
}
