package com.motivity.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class JwtValidationRequest {

    private String jwt;

    @JsonCreator
    public JwtValidationRequest(@JsonProperty("jwt") String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}
